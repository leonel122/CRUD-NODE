var express = require('express');
var app = express();
var user_controller = express.Router();
var model_user = require('../models/user_model');
var bcrypt = require('bcrypt-nodejs');

user_controller.get('/register',(req, res)=>{
    res.render('users/register',{'title': "Registro de Usuarios"});
});

user_controller.post('/register',(req, res)=>{
    var body = req.body;
    body.status = false;
    body.password = bcrypt.hashSync(body.password);
    model_user.create(body, (err, users) => {
        if (!err) throw (err);
        console.log(err);
        res.redirect('/login');
    });
});

user_controller.get('/login',(req, res)=>{
    res.render('users/login',{'title':'Login'});
});

user_controller.post('/login',(req,res)=>{
    console.log(req.body);
    res.send('ok');
});


function errorHandler(err, req, res, next) {
    res.status(500);
    res.render('error', { error: err });
    console.log('error', { error: err });
  }

module.exports = user_controller;