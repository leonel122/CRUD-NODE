//requerimos la conexión
const mongoose = require('../lib/db-connection-mongo');
//guardamos en Schema el metodo de mongoose Schema
var Schema = mongoose.Schema;

//squema del documento de mongo
var user_schema = new Schema({
        email:  {type: String, trim: true, unique: true},
        password: {type: String, trim: true},
        name:  {type: String},
        status :  {type: Boolean}
    });

//exportamos la schema que hicimos de tareas
module.exports = mongoose.model('users', user_schema);
