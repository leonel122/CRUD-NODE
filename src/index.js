const express = require('express');
const morgan = require('morgan');
var body_parser = require('body-parser');

const app = express();
//llamamos al al user_contoller
const user_routes = require('./controller/user_controller');


//ruta principal del proyecto en cualquier sistema operativo
const paht = require('path');


//--------middleware
//DEBE IR DE PRIMERO utilizamos body-parser para los metodos post
app.use(body_parser.urlencoded({extended:true}));
//llamamos el archivo user_controller guardado en user_routes
app.use('/', user_routes);
//utilizamos morgan para impirmir en el servidor la peticion de la ruta
app.use(morgan('tiny'));

//manejo de errores
app.use(errorHandler);

function errorHandler(err, req, res, next) {
  res.status(500);
  res.render('error', { error: err });
}
//-------fin middleware
//----------------configuraciones del servidor
//se configura el nombre de la app
app.set('appName','Crud users');
//se configura el puerto
app.set('port','3000');
app.set('views', paht.join(__dirname+'/views'));
//le decimos al servidor que motor de plantillas va a usar
app.set('view engine', 'ejs');
//----------------fin configuracion del servidor


app.listen(app.get('port'), function () {
  console.log('Example app listening on port '+app.get('port'));
  console.log('Nombre de app '+ app.get('appName'));
});
